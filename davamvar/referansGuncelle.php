

<!doctype html>
<html lang="tr">
  <head>
    <title> Referans Güncelle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/quill.snow.css">
    <!--  CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>
    

<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> 
    <!-- ÜST KISIM -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="avukatIndex.php">Davam Var</a></div>
            </ul>
          </nav>
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="profilGuncelle.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Profil İşlemleri</a>
              <a href="referansEkle.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Referans Ekle</a>
              <a href="referanslarim.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Referans Güncelle</a>
              <a href="index.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Çıkış Yap</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>

        </div>
      </div>
    </header>

    <!-- ANA SAYFA -->
    <section class="section-hero overlay inner-page bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
          </div>
        </div>
      </div>
    </section>

    
    <section class="site-section">
      <div class="container">

        <div class="row align-items-center mb-5">
          <div class="col-lg-8 mb-4 mb-lg-0">
            <div class="d-flex align-items-center">
              <div>
                <h2>Referans Güncelle</h2>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="row">
              <div class="col-6">
              </div>
              <div class="col-6">
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-lg-12">
<?php
	require_once("baglan.php");
	require_once("fonksiyonlar.php");
    session_start();
    $ziyaretci = true;
    if($_SESSION["login"]==true) {
        $avukatid = $_SESSION["avukatid"];
        $ziyaretci = False; 
    }
	if($ziyaretci==true) {
	header("location: index.php") ;
	}
?>



<?php 
		
		$referans_id = $_GET['referans_id'];
		$url = "http://79.143.51.110/plesk-site-preview/odev.app/https/79.143.51.110/users/".$referans_id;                           #bilgiyi almak istediginiz web linki
		$ch = curl_init();                                                                        #curl komutunu baslatiyor
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL, $url);
		// Execute
		$result = curl_exec($ch);
		curl_close($ch);
		curl_close($ch);       	
		$var=json_decode($result);
		$referans_id =$var->referansbilgileri->referans_id;
		$kategori_id =$var->referansbilgileri->kategori_id;
		
		echo ' <form class="p-4 p-md-5 border rounded" method="post" action="referansGuncelle.php">
              <h3 class="text-black mb-5 border-bottom pb-2">Referans Bilgileri</h3>
              <div class="form-group">
                <label for="job-region"> Dava Türü</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Dava Türü Seçin" name="kategori_id">';
	
						$query = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategori_id'", PDO::FETCH_ASSOC);
						if ( $query->rowCount() ){
						foreach( $query as $row ){
								echo '<option selected value="'.$row['kategori_id'].'">'.$row['kategori_ad'].'</option>';
						}}
						
						$query2 = $db->query("SELECT * FROM kategori", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo '<option value="'.$row2['kategori_id'].'">'.$row2['kategori_ad'].'</option>';
						}}
						
                    echo '</select>
              </div>
              <div class="form-group">
              <div class="form-group">
                <label for="job-description">Referans Açıklaması</label>
                <div class="editor">
                  <textarea placeholder="" name="referans_aciklamasi"  cols="124" rows="10">'.$var->referansbilgileri->referans_aciklamasi.'</textarea>
                </div>
              </div>
			 	<input name="guncel" type="hidden" value="ol" />
				<input name="referans_id" type="hidden" value="'.$referans_id .'" />
                <button type="submit" class="btn btn-success floatRight"><a class="btn btn-block btn-primary btn-md" >Referans Güncelle</a></button>
            </form>';
			
	if(isset($_POST['guncel'])) {
			$referans_id = $_POST["referans_id"];
			$kategori_id = $_POST["kategori_id"];
			$referans_aciklamasi = $_POST["referans_aciklamasi"];			

			$sorgu= $db->prepare("UPDATE referans SET 
                            kategori_id = ?,
							referans_aciklamasi = ? WHERE
							referans_id = ?");
			$sorgu->execute([ $kategori_id , $referans_aciklamasi , $referans_id]);
		
		$mesaj = 'Referans Güncellendi!';
		alertMesajVer($mesaj);
		echo '<script> setTimeout( function () { window.location.href = "referanslarim.php"}, 500); </script>';

	}
		
?>  
					
          </div>
        </div>
        <div class="row align-items-center mb-5">
               <div class="col-lg-4 ml-auto">
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

    <!-- SCRIPT KODLARI -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/quill.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>