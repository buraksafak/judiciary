<?php
	require_once("baglan.php");
	require_once("fonksiyonlar.php");
    session_start();
    $ziyaretci = true;
    if($_SESSION["login"]==true) {
        $avukatid = $_SESSION["avukatid"];
        $ziyaretci = False; 
    }
	if($ziyaretci==true) {
	header("location: index.php") ;
	}
	
	if(isset($_POST['guncel'])) {
			$kategori = $_POST["kategori"];
			$sehir = $_POST["sehir"];
			$gizliyanitid = $_POST["gizliyanitid"];
			$ad = $_POST["ad"];
            $soyad = $_POST["soyad"];
			$sifre1 = $_POST["sifre1"];  
			$sifre2 = $_POST["sifre2"]; 
			$mail = $_POST["mail"];
			$telno = $_POST["telno"];
            $hakkinda = $_POST["hakkinda"];
            $adres = $_POST["adres"];		
            $gizliyanit = $_POST["gizliyanit"];
			
	if($sifre1==$sifre2)
			{
			$sorgu= $db->prepare("UPDATE avukat SET 
                            kategori_id = ?,
                            sehir_id = ?,
                            gizli_yanit_id = ?,
                            ad = ?,
                            soyad = ?,
                            sifre = ?,
                            mail = ?,
                            telefon = ?,
							hakkinda = ?,
							adres = ?,
							gizli_yanit = ? WHERE
							avukat_id = ?");
			$sorgu->execute([ $kategori , $sehir , $gizliyanitid, $ad , $soyad, $sifre1, $mail, $telno, $hakkinda, $adres, $gizliyanit, $avukatid ]);

	}
	else {
		$mesaj = 'Şifreler Uyuşmuyor.';
		alertMesajVer($mesaj);
		}
		
	}

?>
<!doctype html>
<html lang="tr">
  <head>
    <title> Profil Güncelle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/quill.snow.css">
    <!--  CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>
    

<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> 
    <!-- ÜST KISIM -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="avukatIndex.php">Davam Var</a></div>
            </ul>
          </nav>
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="profilGuncelle.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Profil İşlemleri</a>
              <a href="referansEkle.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Referans Ekle</a>
              <a href="referanslarim.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Referans Güncelle</a>
              <a href="index.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Çıkış Yap</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>

        </div>
      </div>
    </header>

    <!-- ANA SAYFA -->
    <section class="section-hero overlay inner-page bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
          </div>
        </div>
      </div>
    </section>

    
    <section class="site-section">
      <div class="container">

        <div class="row align-items-center mb-5">
          <div class="col-lg-8 mb-4 mb-lg-0">
            <div class="d-flex align-items-center">
              <div>
                <h2>Profil Güncelle</h2>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="row">
              <div class="col-6">
              </div>
              <div class="col-6">
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-lg-12">
		  
		  	<?php 
			$query = $db->query("SELECT * FROM avukat WHERE avukat_id = '$avukatid'");
			if($query->rowCount()) { // Eğer böyle bir kullanıcı varsa
			$row = $query->fetch(PDO::FETCH_ASSOC);			
			echo '
			<form class="p-4 p-md-5 border rounded" method="post" action="profilGuncelle.php">
              <h3 class="text-black mb-5 border-bottom pb-2">Kişisel Bilgiler</h3>
              <div class="form-group">
                <label for="job-title">Adınız</label>
                <input type="text" class="form-control" name="ad" value="'.$row["ad"].'" placeholder="Örn:Faruk">
              </div>
              <div class="form-group">
                <label for="job-title">Soyadınız</label>
                <input type="text" class="form-control" name="soyad" value="'.$row["soyad"].'" placeholder="Örn:Şafak">
              </div>
              <div class="form-group">
                <label for="email">Mail Adresiniz</label>
                <input type="text" class="form-control" name="mail" value="'.$row["mail"].'" placeholder="ornek@ornekmail.com">
              </div>
              <div class="form-group">
                <label for="job-title">Telefon Numaranız</label>
                <input type="text" class="form-control" name="telno" value="'.$row["telefon"].'" placeholder="Örn : 0 312 999 99 99">
              </div>
              <div class="form-group">
                <label for="job-title">Şifreniz</label>
                <input type="password" class="form-control" name="sifre1" value="'.$row["sifre"].'" placeholder="Şifreniz">
              </div>
              <div class="form-group">
                <label for="job-title">Şifre Tekrar</label>
                <input type="password" class="form-control" name="sifre2" value="'.$row["sifre"].'" placeholder="Şifreniz">
              </div>
              <div class="form-group">
                <label for="job-region">Bulunduğunuz Şehir</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Şehir Seçin" name="sehir">';
						
						$sehirid = $row["sehir_id"];
						$query4 = $db->query("SELECT * FROM sehir WHERE sehir_id='$sehirid'", PDO::FETCH_ASSOC);
						if ( $query4->rowCount() ){
						foreach( $query4 as $row4 ){
								echo '<option selected value="'.$row4['sehir_id'].'">'.$row4['sehir_ad'].'</option>';
						}}
						
						$query5 = $db->query("SELECT * FROM sehir", PDO::FETCH_ASSOC);
						if ( $query5->rowCount() ){
						foreach( $query5 as $row5 ){
								echo '<option value="'.$row5['sehir_id'].'">'.$row5['sehir_ad'].'</option>';
						}}
						
                echo '</select>
              </div>	 
              <div class="form-group">
                <label for="job-description">Adresiniz</label>
                <div class="editor">
                  <textarea placeholder="Örn. Örnek Mah. Örnek Sok. 1/1  ÖRNEK " name="adres" cols="124" rows="10">'.$row["adres"].'</textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="job-region">Uzman Olduğunuz Dava Türü</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Dava Türü Seçin" name="kategori">';

						$kategoriid = $row["kategori_id"];
						$query6 = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategoriid'", PDO::FETCH_ASSOC);
						if ( $query6->rowCount() ){
						foreach( $query6 as $row6 ){
								echo '<option selected value="'.$row6['kategori_id'].'">'.$row6['kategori_ad'].'</option>';
						}}
						
						$query2 = $db->query("SELECT * FROM kategori", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo '<option value="'.$row2['kategori_id'].'">'.$row2['kategori_ad'].'</option>';
						}}
						
                    echo '</select>
              </div>
              <div class="form-group">
                <label for="job-region">Gizli Soru</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Örn : En Sevdiğiniz Öğretmen" name="gizliyanitid">';
						//sıkıntı yok
						
						$gizliyanitid = $row["gizli_yanit_id"];
						$query7 = $db->query("SELECT * FROM gizli_yanit WHERE gizli_yanit_id='$gizliyanitid'", PDO::FETCH_ASSOC);
						if ( $query7->rowCount() ){
						foreach( $query7 as $row7 ){
								echo '<option selected value="'.$row7['gizli_yanit_id'].'">'.$row7['gizli_soru'].'</option>';
						}}
						
						$query3 = $db->query("SELECT * FROM gizli_yanit", PDO::FETCH_ASSOC);
						if ( $query3->rowCount() ){
						foreach( $query3 as $row3 ){
								echo '<option value="'.$row3['gizli_yanit_id'].'">'.$row3['gizli_soru'].'</option>';
						}}
						
                    echo '</select>
                  </div>
                  <div class="form-group">
                    <label for="job-title">Gizli Yanıt</label>
                    <input type="text" class="form-control"  name="gizliyanit" value="'.$row["gizli_yanit"].'" id="job-title" placeholder="Örn : Asım Sinan Yüksel">
                  </div> 
              <div class="form-group">
                <label for="job-description">Hakkınızda</label>
				  <textarea placeholder="Örn: Isparta Süleyman Demirel Üniversitesi Hukuk Fakültesi 2010 mezunuyum" name="hakkinda" id="" cols="124" rows="10">'.$row["hakkinda"].'</textarea>
              </div>
              <div class="form-group">          
				<input name="guncel" type="hidden" value="ol" />
                <button type="submit" class="btn btn-success floatRight"><a class="btn btn-block btn-primary btn-md" >Güncelle</a></button>
				<button type="submit" class="btn btn-success floatRight"><a href="hesapsil.php?avukat_id='.$avukatid.'" class="btn btn-block btn-primary btn-md" >Hesap Sil</a></button>
				</form>'; 
			}
			?>
			
			
			
          </div>
        </div>
        <div class="row align-items-center mb-5">
               <div class="col-lg-4 ml-auto">
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

    <!-- SCRIPT KODLARI -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/quill.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>