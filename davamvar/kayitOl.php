<?php 
require_once("baglan.php");
require_once("fonksiyonlar.php");

if(isset($_POST['kayit'])){
	if($_POST['sifre1']!=$_POST['sifre2']) bildirimGoster('Şifreler Aynı Değil!');
	else {
			$kategori = $_POST["kategori"];
			$sehir = $_POST["sehir"];
			$gizliyanitid = $_POST["gizliyanitid"];
			$ad = $_POST["ad"];
            $soyad = $_POST["soyad"];
			$sifre1 = $_POST["sifre1"];    
			$mail = $_POST["mail"];
			$telno = $_POST["telno"];
            $hakkinda = $_POST["hakkinda"];
            $adres = $_POST["adres"];		
            $gizliyanit = $_POST["gizliyanit"];
            $ekle = $db->prepare('INSERT INTO avukat SET
                            kategori_id = ?,
                            sehir_id = ?,
                            gizli_yanit_id = ?,
                            ad = ?,
                            soyad = ?,
                            sifre = ?,
                            mail = ?,
                            telefon = ?,
							hakkinda = ?,
							adres = ?,
                            gizli_yanit = ?');
            $ekle->execute([ $kategori , $sehir , $gizliyanitid, $ad , $soyad, $sifre1, $mail, $telno, $hakkinda, $adres, $gizliyanit ]);
			$mesaj = 'Üyelik Başarıyla Oluşturuldu.Ana Sayfaya Yönlendiriliyorsunuz..';
			alertMesajVer($mesaj);
            echo '<script> setTimeout( function () { window.location.href = "index.php"}, 5000); </script>';

	}
}

?>


<!doctype html>
<html lang="tr">
  <head>
    <title> Kayıt Ol</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/quill.snow.css">
    <!--  CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>
    

<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> 
    <!-- ÜST KISIM -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="index.php">Davam Var</a></div>
            </ul>
          </nav>
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="kayitOl.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Avukat Hesabı Aç</a>
              <a href="girisYap.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Avukat Gİriş</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>

        </div>
      </div>
    </header>

    <!-- ANA SAYFA -->
    <section class="section-hero overlay inner-page bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
          </div>
        </div>
      </div>
    </section>

    
    <section class="site-section">
      <div class="container">
        <div class="row align-items-center mb-5">
          <div class="col-lg-8 mb-4 mb-lg-0">
            <div class="d-flex align-items-center">
              <div>
                <h2>Kayıt Ol</h2>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="row">
              <div class="col-6">
              </div>
              <div class="col-6">
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-lg-12">
		  
		  
            <form class="p-4 p-md-5 border rounded" method="post" action="kayitOl.php">
              <h3 class="text-black mb-5 border-bottom pb-2">Kişisel Bilgiler</h3>
              <div class="form-group">
                <label for="job-title">Adınız</label>
                <input type="text" class="form-control" name="ad" placeholder="Örn:Faruk">
              </div>
              <div class="form-group">
                <label for="job-title">Soyadınız</label>
                <input type="text" class="form-control" name="soyad" placeholder="Örn:Şafak">
              </div>
              <div class="form-group">
                <label for="email">Mail Adresiniz</label>
                <input type="text" class="form-control" name="mail" placeholder="ornek@ornekmail.com">
              </div>
              <div class="form-group">
                <label for="job-title">Telefon Numaranız</label>
                <input type="text" class="form-control" name="telno" placeholder="Örn : 0 312 999 99 99">
              </div>
              <div class="form-group">
                <label for="job-title">Şifreniz</label>
                <input type="password" class="form-control" name="sifre1" placeholder="Şifreniz">
              </div>
              <div class="form-group">
                <label for="job-title">Şifre Tekrar</label>
                <input type="password" class="form-control" name="sifre2" placeholder="Şifreniz">
              </div>
              <div class="form-group">
                <label for="job-region">Bulunduğunuz Şehir</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Şehir Seçin" name="sehir">
						<?php 		  	
						$query = $db->query("SELECT * FROM sehir", PDO::FETCH_ASSOC);
						if ( $query->rowCount() ){
						foreach( $query as $row ){
								echo '<option value="'.$row['sehir_id'].'">'.$row['sehir_ad'].'</option>';
						}}
						?>
                </select>
              </div>	 
              <div class="form-group">
                <label for="job-description">Adresiniz</label>
                <div class="editor">
                  <textarea placeholder="Örn. Örnek Mah. Örnek Sok. 1/1  ÖRNEK " name="adres"  cols="124" rows="10"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="job-region">Uzman Olduğunuz Dava Türü</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Dava Türü Seçin" name="kategori">
						<?php 		  	
						$query = $db->query("SELECT * FROM kategori", PDO::FETCH_ASSOC);
						if ( $query->rowCount() ){
						foreach( $query as $row ){
								echo '<option value="'.$row['kategori_id'].'">'.$row['kategori_ad'].'</option>';
						}}
						?>
                    </select>
              </div>
              <div class="form-group">
                <label for="job-region">Gizli Soru</label>
                <select class="selectpicker border rounded" id="job-region" data-style="btn-black" data-width="100%" data-live-search="true" title="Örn : En Sevdiğiniz Öğretmen" name="gizliyanitid">
						<?php 		  	
						$query = $db->query("SELECT * FROM gizli_yanit", PDO::FETCH_ASSOC);
						if ( $query->rowCount() ){
						foreach( $query as $row ){
								echo '<option value="'.$row['gizli_yanit_id'].'">'.$row['gizli_soru'].'</option>';
						}}
						?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="job-title">Gizli Yanıt</label>
                    <input type="text" class="form-control" name="gizliyanit" id="job-title" placeholder="Örn : Asım Sinan Yüksel">
                  </div> 
              <div class="form-group">
                <label for="job-description">Hakkınızda</label>
				  <textarea placeholder="Örn: Isparta Süleyman Demirel Üniversitesi Hukuk Fakültesi 2010 mezunuyum" name="hakkinda" id="" cols="124" rows="10"></textarea>
              </div>
              <div class="form-group">          
				<input name="kayit" type="hidden" value="ol" />
                <button type="submit" class="btn btn-success floatRight"><a class="btn btn-block btn-primary btn-md" >Kayıt Ol</a></button>
				</form>              
				</div>
			

          </div>
        </div>
        <div class="row align-items-center mb-5">
               <div class="col-lg-4 ml-auto">			  
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

    <!-- SCRIPT KODLARI -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/quill.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>