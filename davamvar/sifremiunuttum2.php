<?php 
require_once("baglan.php");

	if(isset($_POST['veri'])){
		$mail = $_POST["mail"];			
		$query = $db->query("SELECT gizli_yanit_id FROM avukat WHERE mail = '$mail'");
		$row = $query->fetch(PDO::FETCH_ASSOC);
		$gizliyanitid =  $row['gizli_yanit_id'];	
	}

?>
<!doctype html>
<html lang="tr">
  <head>
    <title>Giriş Yap</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/quill.snow.css">
    

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>
    

<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    

    <!-- NAVBAR -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="index.php">Davam Var</a></div>

          
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="kayitOl.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Avukat Kaydı Aç</a>
              <a href="girisYap.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Giriş Yap</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>
        </div>
      </div>
    </header>

    <!-- HOME -->
    <section class="section-hero overlay inner-page bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
          </div>
        </div>
      </div>
    </section>
    <section class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5">
              </div>
            </form>
          </div>
          <div class="col-lg-6">
            <h2 class="mb-4">Şifre Gör</h2>
			
            <form action="sifremiunuttum3.php" method="post" class="p-4 border rounded">
              <div class="row form-group mb-4">
                <div class="col-md-12 mb-3 mb-md-0">
                  <p>
				  <?php 
							$query2 = $db->query("SELECT gizli_soru FROM gizli_yanit WHERE gizli_yanit_id = '$gizliyanitid'");
							$row2 = $query2->fetch(PDO::FETCH_ASSOC);
							echo $row2['gizli_soru'];
							echo '<input type="hidden" name="mail" value="'.$mail.'">';
				  ?>
				  </p>
                </div>
                <div class="col-md-12 mb-3 mb-md-0">
                  <input type="text" id="fname" class="form-control" placeholder="Gizli Yanıtınızı Yazınız" name="yanit">
                </div>
              </div>
              <div class="row form-group mb-4">
                <div class="col-md-12 mb-3 mb-md-0">
				<input name="dogru" type="hidden" value="mu" />
                <button type="submit" class="btn btn-success floatRight"><a class="btn btn-block btn-primary btn-md" >Şifreyi Gör</a></button>                </div>
                </div>
              </div>
            </form>
			
			
          </div>
        </div>
      </div>
    </section>
    <footer class="site-footer">
      <a href="#top" class="smoothscroll scroll-top">
        <span class="icon-keyboard_arrow_up"></span>
      </a>
      <div class="container">
        <div class="row mb-5">
          <div class="col-6 col-md-3 mb-4 mb-md-0">
          </div>
        </div>
      </div>
    </footer>
  </div>

    <!-- SCRIPT KODLARI -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/quill.min.js"></script>
    
    
    <script src="js/bootstrap-select.min.js"></script>
    
    <script src="js/custom.js"></script>
   
   
     
  </body>
</html>