
<!doctype html>
<html lang="tr">
  <head>
    <title>Ana Sayfa</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Free-Template.co" />
    <link rel="shortcut icon" href="ftco-32x32.png">
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>    
<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    <!-- NAVBAR -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="index.php">Davam Var</a></div>
            </ul>
          </nav>
          
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="kayitOl.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Avukat Hesabı Aç</a>
              <a href="girisYap.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Avukat Giriş</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>

        </div>
      </div>
    </header>
    <!-- HOME -->
    <section class="home-section section-hero overlay bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-12">
            <div class="mb-5 text-center">
              <h1 class="text-white font-weight-bold">AVUKAT ARA</h1>
              <p>Avukat ismine , bulunduğu şehire veya dava kategorisine göre arama yapabilirsiniz.</p>
            </div>
			
			
            <form method="post" action="avukatlistele.php" class="search-jobs-form">
              <div class="row mb-5">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                  <input type="text" class="form-control form-control-lg" placeholder="Avukat Adı Soyadı" name="adsoyad">
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                  <select name="sehir" class="selectpicker" data-style="btn-white btn-lg" data-width="100%" data-live-search="true" title="Şehir Seçiniz">
						<?php 	
						require_once("baglan.php");
						$query = $db->query("SELECT * FROM sehir", PDO::FETCH_ASSOC);
						if ( $query->rowCount() ){
						foreach( $query as $row ){
								echo '<option value="'.$row['sehir_id'].'">'.$row['sehir_ad'].'</option>';
						}}
						?>
                  </select>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
                  <select name="kategori" class="selectpicker" data-style="btn-white btn-lg" data-width="100%" data-live-search="true" title="Dava Kategorisi">
						<?php 		  	
						$query = $db->query("SELECT * FROM kategori", PDO::FETCH_ASSOC);
						if ( $query->rowCount() ){
						foreach( $query as $row ){
								echo '<option value="'.$row['kategori_id'].'">'.$row['kategori_ad'].'</option>';
						}}
						?>
                  </select>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0">
					<input name="listele" type="hidden" value="evet" />
					<button type="submit" class="btn btn-success floatRight"><a class="btn btn-block btn-primary btn-md" >Listele</a></button>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 popular-keywords">
                  <h3>Örneğin:</h3> 
                  <ul class="keywords list-unstyled m-0 p-0">
                    <li><a href="#" class="">Faruk ATASOY</a></li>
                    <li><a href="#" class="">İzmir</a></li>
                    <li><a href="#" class="">Ticari Davalar</a></li>
                  </ul>
                </div>
              </div>
            </form>
			
			
			
          </div>
        </div>
      </div>
</section>
</div>
    </section>
<section class="site-section">
      <div class="container">

        
      </div>
    </footer>
  
  </div>

    <!-- SCRIPTS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    
    <script src="js/bootstrap-select.min.js"></script>
    
    <script src="js/custom.js"></script>

     
  </body>
</html>