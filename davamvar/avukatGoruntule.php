<?php
	require_once("baglan.php");
    if(!isset($_GET['avukatid'])) {
        header("location: index.php");
    }
?>
<!doctype html>
<html lang="tr">
  <head>
    <title>JobBoard &mdash; Website Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>
    

<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
  
    <!-- NAVBAR -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="index.php">Davam Var</a></div>
          
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="kayitOl.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Avukat Kaydı Aç</a>
              <a href="girisYap.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Giriş Yap</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>

        </div>
      </div>
    </header>

    <!-- HOME -->
    <section class="section-hero overlay inner-page bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">
      <div class="container">
        <div class="row">
          </div>
        </div>
      </div>
    </section>
	
	
	<?php
	if(isset($_GET['avukatid'])) {
	$avukatid = $_GET["avukatid"];	
	$query = $db->query("SELECT * FROM avukat WHERE avukat_id = '$avukatid'"); // Veritabanında bilgileri arıyoruz.
	$row = $query->fetch(PDO::FETCH_ASSOC);

    echo '<section class="site-section">
      <div class="container">
        <div class="row align-items-center mb-5">
          <div class="col-lg-8 mb-4 mb-lg-0">
            <div class="d-flex align-items-center">
              <div>
                <h2>'.$row['ad'].' '.$row['soyad'].'</h2>
                <div>
                  <span class="ml-0 mr-2 mb-2"><span class="icon-briefcase mr-2"></span>';
						$kategoriid = $row["kategori_id"];
						$query1 = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategoriid'", PDO::FETCH_ASSOC);
						if ( $query1->rowCount() ){
						foreach( $query1 as $row1 ){
								echo $row1['kategori_ad'];
						}}				  				  
				  echo '</span>
                  <span class="m-2"><span class="icon-room mr-2"></span>';			  
				  		$sehirid = $row["sehir_id"];
						$query2 = $db->query("SELECT * FROM sehir WHERE sehir_id='$sehirid'", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo $row2['sehir_ad'];
						}}	  
				  echo '</span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="row">
              <div class="col-6">
                <a href="referanslar.php?avukatid='.$avukatid.'" class="btn btn-block btn-primary btn-md">Referanslar</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8">
            <div class="mb-5">
              <figure class="mb-5"><img src="images/adalet.jpg" alt="Image" class="img-fluid rounded"></figure>
            </div>
            <div class="mb-5">
              <h3 class="h5 d-flex align-items-center mb-4 text-primary"><span class="icon-book mr-3"></span>Hakkımda</h3>
              <p> '.$row['hakkinda'].'</p>
              <ul class="list-unstyled m-0 p-0">
              </ul>
            </div>

          </div>
          <div class="col-lg-4">
            <div class="bg-light p-3 border rounded mb-4">
              <h3 class="text-primary  mt-3 h5 pl-3 mb-3 ">Kişisel Bilgiler</h3>
              <ul class="list-unstyled pl-3 mb-0">
                <li class="mb-2"><strong class="text-black">Adı : </strong> '.$row['ad'].'</li>
                <li class="mb-2"><strong class="text-black">Soyadı : </strong> '.$row['soyad'].'</li>
                <li class="mb-2"><strong class="text-black">Mail Adresi :</strong> '.$row['mail'].'</li>
                <li class="mb-2"><strong class="text-black">Telefon Numarası : </strong> '.$row['telefon'].'</li>
                <li class="mb-2"><strong class="text-black">Şehir : </strong>';
				  		$sehirid = $row["sehir_id"];
						$query2 = $db->query("SELECT * FROM sehir WHERE sehir_id='$sehirid'", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo $row2['sehir_ad'];
						}}	  
				echo '</li>
                <li class="mb-2"><strong class="text-black">Adresi :</strong> '.$row['adres'].'</li>
                <li class="mb-2"><strong class="text-black">Uzmanlık Alanı :</strong>';
						$kategoriid = $row["kategori_id"];
						$query1 = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategoriid'", PDO::FETCH_ASSOC);
						if ( $query1->rowCount() ){
						foreach( $query1 as $row1 ){
								echo $row1['kategori_ad'];
						}}				  						
				echo '</li>
              </ul>
            </div>


          </div>
        </div>
      </div>
    </section>';
	}
	?>


    <footer class="site-footer">

      <a href="#top" class="smoothscroll scroll-top">
        <span class="icon-keyboard_arrow_up"></span>
      </a>
    </footer>
  
  </div>

    <!-- SCRIPTS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    
    <script src="js/bootstrap-select.min.js"></script>
    
    <script src="js/custom.js"></script>

     
  </body>
</html>