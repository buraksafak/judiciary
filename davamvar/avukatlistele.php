
<!doctype html>
<html lang="en">
  <head>
    <title>JobBoard &mdash; Website Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link rel="stylesheet" href="css/custom-bs.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="fonts/line-icons/style.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/quill.snow.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body id="top">

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
    </div>
  </div>
<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    <!-- NAVBAR -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="index.php">Davam Var</a></div> 
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="kayitOl.php" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Avukat Hesabı Aç</a>
              <a href="girisYap.php" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Giriş Yap</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>

        </div>
      </div>
    </header>
    <!-- HOME -->
  <section class="section-hero home-section overlay inner-page bg-image" style="background-image: url('images/hero_1.jpg');" id="home-section">
      <div class="container">
        <div class="row align-items-center justify-content-center">
        </div>
      </div>
    </section>
    <section class="site-section" id="next">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7 text-center">
            <h2 class="section-title mb-2">Listelenen Avukatlar</h2>
          </div>
        </div>       
        <ul class="job-listings mb-5">
		
		<?php
		require_once("baglan.php");
		require_once("fonksiyonlar.php");
		if(isset($_POST['listele'])){
			$adsoyad = $_POST["adsoyad"];
			$kategori = $_POST["kategori"];
			$sehir = $_POST["sehir"];
			
		if($adsoyad!=""){
			$dizi = explode (" ",$adsoyad);
			$ad = $dizi[0];
			$soyad = $dizi[1];		
		}
		
		if(isset($ad)&&isset($soyad)&&isset($sehir)&&isset($kategori)) {
			$query = $db->query("SELECT * FROM avukat WHERE ad='$ad' AND soyad='$soyad' AND kategori_id='$kategori' AND sehir_id='$sehir'", PDO::FETCH_ASSOC);
			if ( $query->rowCount() ){
			foreach( $query as $row ){
		echo '
          <li class="job-listing d-block d-sm-flex pb-3 pb-sm-0 align-items-center">
            <a href="avukatGoruntule.php?avukatid='.$row['avukat_id'].'"></a>
            <div class="job-listing-logo">
              <img src="images/adalet.jpg" alt="Image" class="img-fluid">
            </div>
            <div class="job-listing-about d-sm-flex custom-width w-100 justify-content-between mx-4">
              <div class="job-listing-position custom-width w-50 mb-3 mb-sm-0">
                <h2>'.$row['ad'].' '.$row['soyad'].' </h2>
                <strong>';
						$kategoriid = $row["kategori_id"];
						$query1 = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategoriid'", PDO::FETCH_ASSOC);
						if ( $query1->rowCount() ){
						foreach( $query1 as $row1 ){
								echo $row1['kategori_ad'];
						}}
				echo '</strong>
              </div>
              <div class="job-listing-location mb-3 mb-sm-0 custom-width w-25">
                <span class="icon-room"></span>';
						$sehirid = $row["sehir_id"];
						$query2 = $db->query("SELECT * FROM sehir WHERE sehir_id='$sehirid'", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo $row2['sehir_ad'];
						}}
              echo '</div>
              <div class="job-listing-meta">
                <span class="badge badge-success">5 Referans</span>
              </div>
            </div>
          </li>
		  ';
		}}}
		if(!isset($ad) && !isset($soyad)) {
        			$query = $db->query("SELECT * FROM avukat WHERE kategori_id='$kategori' AND sehir_id='$sehir'", PDO::FETCH_ASSOC);
			if ( $query->rowCount() ){
			foreach( $query as $row ){
		echo '
          <li class="job-listing d-block d-sm-flex pb-3 pb-sm-0 align-items-center">
            <a href="avukatGoruntule.php?avukatid='.$row['avukat_id'].'"></a>
            <div class="job-listing-logo">
              <img src="images/adalet.jpg" alt="Image" class="img-fluid">
            </div>
            <div class="job-listing-about d-sm-flex custom-width w-100 justify-content-between mx-4">
              <div class="job-listing-position custom-width w-50 mb-3 mb-sm-0">
                <h2>'.$row['ad'].' '.$row['soyad'].' </h2>
                <strong>';
						$kategoriid = $row["kategori_id"];
						$query1 = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategoriid'", PDO::FETCH_ASSOC);
						if ( $query1->rowCount() ){
						foreach( $query1 as $row1 ){
								echo $row1['kategori_ad'];
						}}
				echo '</strong>
              </div>
              <div class="job-listing-location mb-3 mb-sm-0 custom-width w-25">
                <span class="icon-room"></span>';
						$sehirid = $row["sehir_id"];
						$query2 = $db->query("SELECT * FROM sehir WHERE sehir_id='$sehirid'", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo $row2['sehir_ad'];
						}}
              echo '</div>
              <div class="job-listing-meta">
                <span class="badge badge-success">';
						$avukat_id = $row['avukat_id'];
						$query2 = $db->query("SELECT  count(*) FROM referans WHERE avukat_id='$avukat_id'", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo "Referans Sayısı: ".$row2['count(*)'];
						}}
				echo '</span>
              </div>
            </div>
          </li>
		  ';
		 }}}
		 

		if(!isset($kategori)) {
        			$query = $db->query("SELECT * FROM avukat WHERE ad='$ad' AND soyad='$soyad' AND sehir_id='$sehir'", PDO::FETCH_ASSOC);
			if ( $query->rowCount() ){
			foreach( $query as $row ){
		echo '
          <li class="job-listing d-block d-sm-flex pb-3 pb-sm-0 align-items-center">
            <a href="avukatGoruntule.php?avukatid='.$row['avukat_id'].'"></a>
            <div class="job-listing-logo">
              <img src="images/adalet.jpg" alt="Image" class="img-fluid">
            </div>
            <div class="job-listing-about d-sm-flex custom-width w-100 justify-content-between mx-4">
              <div class="job-listing-position custom-width w-50 mb-3 mb-sm-0">
                <h2>'.$row['ad'].' '.$row['soyad'].' </h2>
                <strong>';
						$kategoriid = $row["kategori_id"];
						$query1 = $db->query("SELECT * FROM kategori WHERE kategori_id='$kategoriid'", PDO::FETCH_ASSOC);
						if ( $query1->rowCount() ){
						foreach( $query1 as $row1 ){
								echo $row1['kategori_ad'];
						}}
				echo '</strong>
              </div>
              <div class="job-listing-location mb-3 mb-sm-0 custom-width w-25">
                <span class="icon-room"></span>';
						$sehirid = $row["sehir_id"];
						$query2 = $db->query("SELECT * FROM sehir WHERE sehir_id='$sehirid'", PDO::FETCH_ASSOC);
						if ( $query2->rowCount() ){
						foreach( $query2 as $row2 ){
								echo $row2['sehir_ad'];
						}}
              echo '</div>
              <div class="job-listing-meta">
                <span class="badge badge-success">5 Referans</span>
              </div>
            </div>
          </li>
		  ';
		 }}}
		
		}
		?>		 
		  
        </ul>
      </div>
    </section>
    <footer class="site-footer">
      <a href="#top" class="smoothscroll scroll-top">
        <span class="icon-keyboard_arrow_up"></span>
      </a>
    </footer>
  </div>
    <!-- SCRIPT KODLARI -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/stickyfill.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/quill.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>