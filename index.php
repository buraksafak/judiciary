<?php
include "db.php";
include "function.php";
$islem = isset($_GET["islem"]) ? addslashes(trim($_GET["islem"])) : null;
$jsonArray = array(); // array değişkenimiz bunu en alta json objesine çevireceğiz. 
$jsonArray["hata"] = FALSE; // Başlangıçta hata yok olarak kabul edelim. 

$_code = 200; // HTTP Ok olarak durumu kabul edelim. 

	
    // üye ekleme kısmı burada olacak. CREATE İşlemi 
 if($_SERVER['REQUEST_METHOD'] == "POST") {
 	
 	// verilerimizi post yöntemi ile alalım. 
    $kategori_id = addslashes($_POST["kategori_id"]);
    $avukat_id = addslashes($_POST["avukat_id"]);
    $referans_aciklamasi = addslashes($_POST["referans_aciklamasi"]);
    
    // Kontrollerimizi yapalım.
    // gelen kullanıcı adı veya e-posta veri tabanında kayıtlı mı kontrol edelim. 
	//referans_id=kullanıcı adı -- avukat_id=posta
    $referans = $db->query("SELECT * from referans WHERE referans_id='$referans_id' OR avukat_id='$avukat_id'");
    
    if(empty($kategori_id) || empty($avukat_id) || empty($referans_aciklamasi)) {
    	$_code = 400; 
		$jsonArray["hata"] = TRUE; // bir hata olduğu bildirilsin.
        $jsonArray["hataMesaj"] = "Boş Alan Bırakmayınız."; // Hatanın neden kaynaklı olduğu belirtilsin.
	}
	else {
    	
			$ex = $db->prepare("insert into referans set 
			kategori_id= :kat, 
			avukat_id= :av, 
			referans_aciklamasi= :ref");
		$ekle = $ex->execute(array(
			"kat" => $kategori_id,
			"av" => $avukat_id,
			"ref" => $referans_aciklamasi			
		));
		
		
		if( $ekle) {
			$_code = 201;
			$jsonArray["mesaj"] = "Ekleme Başarılı.";
		}else {
			$_code = 400;
			 $jsonArray["hata"] = TRUE; // bir hata olduğu bildirilsin.
       		 $jsonArray["hataMesaj"] = "Sistem Hatası.";
		}
	}
}else if($_SERVER['REQUEST_METHOD'] == "PUT") {
     $gelen_veri = json_decode(file_get_contents("php://input")); // veriyi alıp diziye atadık.
    	
    	// basitçe bi kontrol yaptık veriler varmı yokmu diye 
     if(
			isset($gelen_veri->referans_id) && 
     		isset($gelen_veri->kategori_id) && 
     		isset($gelen_veri->avukat_id) && 
     		isset($gelen_veri->referans_aciklamasi)
     	) {
     		
     		// veriler var ise güncelleme yapıyoruz.
				$q = $db->prepare("UPDATE referans SET kategori_id= :kat, avukat_id= :av, referans_aciklamasi= :ref WHERE referans_id= :rid ");
			 	$update = $q->execute(array(
			 			"kat" => $gelen_veri->kategori_id,
						"av" => $gelen_veri->avukat_id,
			 			"ref" => $gelen_veri->referans_aciklamasi,
			 			"rid" => $gelen_veri->referans_id 	
			 	));
			 	// güncelleme başarılı ise bilgi veriyoruz. 
			 	if($update) {
			 		$_code = 200;
			 		$jsonArray["mesaj"] = "Güncelleme Başarılı";
			 	}
			 	else {
			 		// güncelleme başarısız ise bilgi veriyoruz. 
			 		$_code = 400;
					$jsonArray["hata"] = TRUE;
		 			$jsonArray["hataMesaj"] = "Sistemsel Bir Hata Oluştu";
				}
		}else {
			// gerekli veriler eksik gelirse apiyi kulanacaklara hangi bilgileri istediğimizi bildirdik. 
			$_code = 400;
			$jsonArray["hata"] = TRUE;
	 		$jsonArray["hataMesaj"] = "referans_id,kategori_id,avukat_id,referans_aciklamasi Verilerini json olarak göndermediniz.";
		}
} else if($_SERVER['REQUEST_METHOD'] == "DELETE") {

    // üye silme işlemi burada olacak. DELETE işlemi 
    if(isset($_GET["referans_id"]) && !empty(trim($_GET["referans_id"]))) {
		$referansid = intval($_GET["referans_id"]);
		$referansVarMi = $db->query("select * from referans where referans_id='$referansid'")->rowCount();
		if($referansVarMi) {
			
			$sil = $db->query("delete from referans where referans_id='$referansid'");
			if( $sil ) {
				$_code = 200;
				$jsonArray["mesaj"] = "Üyelik Silindi.";
			}else {
				// silme başarısız ise bilgi veriyoruz. 
				$_code = 400;
				$jsonArray["hata"] = TRUE;
	 			$jsonArray["hataMesaj"] = "Sistemsel Bir Hata Oluştu";
			}
		}else {
			$_code = 400; 
			$jsonArray["hata"] = TRUE; // bir hata olduğu bildirilsin.
    		$jsonArray["hataMesaj"] = "Geçersiz id "; // Hatanın neden kaynaklı olduğu belirtilsin.
		}
	}else {
		$_code = 400;
		$jsonArray["hata"] = TRUE; // bir hata olduğu bildirilsin.
    	$jsonArray["hataMesaj"] = "Lütfen referans_id değişkeni gönderin".$referansid; // Hatanın neden kaynaklı olduğu belirtilsin.
	}
} else if($_SERVER['REQUEST_METHOD'] == "GET") {


    // üye bilgisi listeleme burada olacak. GET işlemi 
    if(isset($_GET["referans_id"]) && !empty(trim($_GET["referans_id"]))) {
		$referansid = intval($_GET["referans_id"]);
		$referansVarMi = $db->query("select * from referans where referans_id='$referansid'")->rowCount();
		if($referansVarMi) {
			
			$bilgiler = $db->query("select * from  referans where referans_id='$referansid'")->fetch(PDO::FETCH_ASSOC);
			$jsonArray["referansbilgileri"] = $bilgiler;
			$_code = 200;
			
		}else {
			$_code = 400;
			$jsonArray["hata"] = TRUE; // bir hata olduğu bildirilsin.
    		$jsonArray["hataMesaj"] = "Referans bulunamadı"; // Hatanın neden kaynaklı olduğu belirtilsin.
		}
	}else {
		$_code = 400;
		$jsonArray["hata"] = TRUE; // bir hata olduğu bildirilsin.
    	$jsonArray["hataMesaj"] = "Lütfen referans_id değişkeni gönderin"; // Hatanın neden kaynaklı olduğu belirtilsin.
	}
}else {
	$_code = 406;
	$jsonArray["hata"] = TRUE;
 	$jsonArray["hataMesaj"] = "Geçersiz method!";
}


SetHeader($_code);
$jsonArray[$_code] = HttpStatus($_code);
echo json_encode($jsonArray);
?>